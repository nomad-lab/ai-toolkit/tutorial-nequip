ARG BUILDER_BASE_IMAGE=jupyter/base-notebook:python-3.9
FROM $BUILDER_BASE_IMAGE


# ================================================================================
# Linux applications and libraries
# ================================================================================

USER root
RUN apt update --yes \
 && apt install --yes --quiet --no-install-recommends \
    build-essential \
    cmake \
    git \
 && apt clean \
 && rm -rf /var/lib/apt/lists/*


# ================================================================================
# Python environment
# ================================================================================

RUN pip install --no-cache-dir --extra-index-url https://download.pytorch.org/whl/cpu \
    'torch==1.12.1' \
    'torchvision' \
    'torchaudio' \
    'nequip' \
    'pandas' \
    'scikit-learn' \
    'lammps_logfile' \
    'ipywidgets' \
    'IProgress' \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"


# ================================================================================
# LAMMPS (https://docs.lammps.org/)
# ================================================================================

WORKDIR /tmp

# Build LAMMPS from github
RUN git clone --depth 1 https://github.com/lammps/lammps.git

RUN mkdir lammps/build-serial \
 && cd lammps/build-serial \
 && cmake -D CMAKE_INSTALL_PREFIX=/opt/lammps \
          -D LAMMPS_MACHINE=serial \
          -D LAMMPS_EXCEPTIONS=yes \
          -D BUILD_MPI=no \
          -D PKG_MANYBODY=yes\
          -D BUILD_LIB=yes \
          -D BUILD_SHARED_LIBS=yes \
          ../cmake \
 && make -j 4 \
 && make install \
 && make install-python \
 && chown -R ${NB_UID}:${NB_GID} /opt/lammps/ \
 && chown -R ${NB_UID}:${NB_GID} /opt/conda/lib/python3.9/site-packages/lammps* \
 && cd /tmp \
 && rm -rf lammps

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/lammps/lib/
ENV PATH=$PATH:/opt/lammps/bin/


# ================================================================================
# Switch back to jovyan to avoid accidental container runs as root
# ================================================================================

WORKDIR ${HOME}
USER ${NB_UID}

COPY --chown=${NB_UID}:${NB_GID} data/ data/
COPY --chown=${NB_UID}:${NB_GID} nequip.ipynb .
