# tutorial-nequip

Build docker image locally:
```
docker build -t gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkits/tutorial-nequip .
```

Push the image to the registry:
```
docker login gitlab-registry.mpcdf.mpg.de
docker push gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkits/tutorial-nequip
```


Run docker image:
```
docker run --rm -v $PWD:/home/jovyan/ -p 8888:8888 gitlab-registry.mpcdf.mpg.de/nomad-lab/ai-toolkits/tutorial-nequip
```